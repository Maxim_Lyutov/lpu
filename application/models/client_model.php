<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client_model extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_spisok()
	{
		return ($this->db->get('client')) ->result_array();
	}
	
	public function get_client($id)
	{
		return ($this->db->get_where('client', array('id'=> $id)))->row_array();
	}

	public function search($params)
	{   
		//return ($this->db->get_where('client', $params))->result_array();
		print_r($params);
		
		$this->db->select('*');
        $this->db->from('client');
        $this->db->join('addres', 'addres.id = client.addres_id');
        $this->db->join('doc_client', 'doc_client.id = client.doc_client_id');
        $this->db->join('document', 'document.id = doc_client.doc_id');
        $this->db->join('polis_client', 'polis_client.id = client.polis_id');
        
		$this->db->where($params);
		
		$query = $this->db->get();
		return $query->result_array(); 
	}

}
