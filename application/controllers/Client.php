<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {
   
	public function __construct()
	{
	    parent::__construct();
		$this->load->model('client_model');
		$this->load->helper(array('form', 'url', 'date'));
	}
	
	public function index()
	{   
	    $data['title'] = 'Список'; 
		$data['data'] = $this->client_model->get_spisok(); 
	    $this->load->view('template/header', $data);
		$this->load->view('client', $data);
		$this->load->view('template/footer');
	}
	
	public function get($id = NULL)
	{   
	    $data['title'] = 'Поиск';
	    $data['client']= $this->client_model->get_client($id);
        $this->load->view('template/header', $data);
		$this->load->view('view', $data);
		$this->load->view('template/footer');		
	}
	
	public function search()
	{   
	    $data['title'] = 'Поиск';
		$this->load->view('template/header', $data);
		
		$params = $this->input->post(array('surname', 'name', 'second_name' , 'birthday'), TRUE);
		$params = array_diff($params, array(''));
		
		$data['client'] = $this->client_model->search($params);
        $this->load->view('search/form', $data);
		$this->load->view('template/footer');
	}
}
