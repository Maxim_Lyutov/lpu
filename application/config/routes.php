<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['search'] = 'search/form';

$route['default_controller'] = 'client';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
