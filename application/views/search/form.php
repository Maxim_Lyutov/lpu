<?php echo validation_errors(); ?>

<?php echo form_open('client/search') ?>
     <label for="surname">surname</label>
     <input type="input" name="surname" value="<?php echo set_value('surname'); ?>" />
   <br/>
     <label for="name">name</label>
     <input type="input" name="name" value="<?php echo set_value('name'); ?>"/>
   <br/>
     <label for="second_name">second_name</label>
     <input type="input" name="second_name" value="<?php echo set_value('second_name'); ?>"/>
   <br/>
	 <label for="birthday">birthday</label>
     <input type="input" name="birthday" value="<?php echo set_value('birthday'); ?>"/>
   <br/>
    <input type="submit" name="submit" value="Поиск" />
</form>

<hr>
<?php if ( !empty($client)): ?>
	<table border = '1'>
	   <tr><th>surname</th>
		   <th>name</th>
		   <th>second_name</th>
		   <th>birthday</th>
		   
		   <th>street</th>
		   <th>build</th>
		   <th>location</th>
		   
		   <th>name_doc</th>
		   <th>seria_doc</th>
		   <th>num_doc</th>
		   <th>who_create_doc</th>
		   
		   <th>seria_polis</th>
		   <th>num_polis</th>
		   <th>end_date_polis</th>
		</tr>   
	<?php foreach ($client as $item):?>
		 <tr>
		   <td><?= $item['surname'] ?></td>
		   <td><?= $item['name'] ?></td>
		   <td><?= $item['second_name'] ?></td>
		   <td><?= $item['birthday'] ?></td>
		   
		   <td><?= $item['street'] ?></td>
		   <td><?= $item['build'] ?></td>
		   <td><?= $item['location'] ?></td>
		   
		   <td><?= $item['name_doc'] ?></td>
		   <td><?= $item['seria_doc'] ?></td>
		   <td><?= $item['num_doc'] ?></td>
		   <td><?= $item['who_create_doc'] ?></td>
		   
		   <td><?= $item['seria_polis'] ?></td>
		   <td><?= $item['num_polis'] ?></td>
		   <td><?= $item['end_date_polis'] ?></td>
		   
		  
		 </tr>

	<?php endforeach;?>
    </table>
<?php endif;?>