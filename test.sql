-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 15, 2019 at 09:55 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `test`;

-- --------------------------------------------------------

--
-- Table structure for table `addres`
--

CREATE TABLE IF NOT EXISTS `addres` (
  `id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `street` varchar(100) NOT NULL,
  `build` int(11) NOT NULL,
  `location` varchar(5) NOT NULL,
  `tmo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `addres`
--

INSERT INTO `addres` (`id`, `region_id`, `area_id`, `street`, `build`, `location`, `tmo_id`) VALUES
(2, 1, 2, 'Бутлерова', 20, 'п112', 5),
(1, 1, 2, 'Седова', 4, 'п8', 9),
(3, 1, 5, 'Седова', 14, 'п8', 9),
(4, 1, 7, 'Бухарестская', 104, 'п102', 3),
(5, 1, 4, 'Ленинский пр', 101, 'п10', 2);

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE IF NOT EXISTS `area` (
  `id` int(11) NOT NULL,
  `name_area` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `name_area`) VALUES
(2, 'Калининский район'),
(5, 'Невский район'),
(4, 'Красносельский район'),
(7, 'Фрунзенский район');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL,
  `surname` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `second_name` varchar(250) NOT NULL,
  `birthday` date NOT NULL,
  `year` int(7) NOT NULL DEFAULT '0',
  `addres_id` int(11) NOT NULL,
  `polis_id` int(11) NOT NULL,
  `doc_client_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `surname`, `name`, `second_name`, `birthday`, `year`, `addres_id`, `polis_id`, `doc_client_id`) VALUES
(1, 'Иванов', 'Иван', 'Иванович', '1986-10-14', 0, 4, 1, 1),
(2, 'Семёнов', 'Семён', 'Семёнович', '1989-09-04', 0, 1, 2, 2),
(3, 'Петров', 'Петр', 'Петрович', '1983-06-14', 0, 1, 3, 3),
(4, 'Иванов', 'Семен', 'Петрович', '1989-10-15', 0, 2, 4, 4),
(5, 'Петров', 'Иван', 'Иванович', '1984-10-15', 0, 3, 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE IF NOT EXISTS `document` (
  `id` int(4) NOT NULL,
  `name_doc` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`id`, `name_doc`) VALUES
(1, 'Паспорт Рф'),
(2, 'Военный паспорт'),
(3, 'Иностранный паспорт'),
(5, 'Справка');

-- --------------------------------------------------------

--
-- Table structure for table `doc_client`
--

CREATE TABLE IF NOT EXISTS `doc_client` (
  `id` int(11) NOT NULL,
  `doc_id` int(4) NOT NULL,
  `seria_doc` varchar(10) NOT NULL,
  `num_doc` varchar(20) NOT NULL,
  `create_doc` date NOT NULL,
  `who_create_doc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doc_client`
--

INSERT INTO `doc_client` (`id`, `doc_id`, `seria_doc`, `num_doc`, `create_doc`, `who_create_doc`) VALUES
(1, 1, '6500', '567878', '2010-02-01', 'УВД г. Москвы и Московской области...'),
(2, 1, '4400', '456723', '2009-10-14', 'УВД г. Санкт-Петербург и ЛО '),
(3, 1, '5686', '457899', '2001-10-14', 'УВД г. Балашова и Балашовского района'),
(4, 1, '1231', '6876868', '2009-10-01', 'УВД п.Космос'),
(5, 2, '1111111111', '22222222222', '2016-10-01', 'УВД Красноярского края отделение 10');

-- --------------------------------------------------------

--
-- Table structure for table `polis_client`
--

CREATE TABLE IF NOT EXISTS `polis_client` (
  `id` int(11) NOT NULL,
  `smo_id` int(11) NOT NULL,
  `seria_polis` varchar(10) NOT NULL,
  `num_polis` varchar(20) NOT NULL,
  `begin_date_polis` date NOT NULL,
  `end_date_polis` date NOT NULL,
  `comment` text NOT NULL,
  KEY `smo_id` (`smo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `polis_client`
--

INSERT INTO `polis_client` (`id`, `smo_id`, `seria_polis`, `num_polis`, `begin_date_polis`, `end_date_polis`, `comment`) VALUES
(1, 3, 'EE', '66123135665756', '2017-10-14', '2022-10-14', ''),
(2, 1, 'EW', '551574362234', '2010-10-14', '2029-10-14', '!!'),
(3, 2, 'EE', '234236462353534', '2010-10-14', '2019-10-20', 'скоро окончание полиса !'),
(4, 3, 'EE', '66123135665', '2017-10-14', '2022-10-14', ''),
(5, 1, 'EW', '551574362234', '2010-10-14', '2029-10-14', '!!');

-- --------------------------------------------------------

--
-- Table structure for table `smo`
--

CREATE TABLE IF NOT EXISTS `smo` (
  `id` int(11) NOT NULL,
  `name_smo` varchar(50) NOT NULL,
  `name_long_smo` text NOT NULL,
  `inn` varchar(20) NOT NULL,
  `kpp` varchar(25) NOT NULL,
  `bik` varchar(25) NOT NULL,
  `adress` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `smo`
--

INSERT INTO `smo` (`id`, `name_smo`, `name_long_smo`, `inn`, `kpp`, `bik`, `adress`) VALUES
(1, 'СК Роcсгострах', 'Страховая компания \"Роcсгострах\"', '111111111', '2019-10-14', '2019-10-14', ''),
(2, 'СК Ингосстрах', 'Страховая Компания \"Ингосстрах\"', '2222222', '6666666666666', '777777777777777', ''),
(3, 'СК ВСК', 'Страховая компания Страховой Дом ВСК', '', '', '', '');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
